/**
 * En abstraksjon av en fysisk alarm. Er ansvarlig for kommunikasjon
 * med en eventuell fysisk alarm; men i vårt eksempel skriver den bare
 * ut beskjeder til output.
 * 
 * @author Torstein Strømme
 */
public class Alarm {
    boolean isActive;

    /**
     * Sett alarmen av (<code>false</code>) eller på (<code>true</code>)
     * @param value <code>true</code> slår alarmen på, <code>false</code> slår den av.
     */
    void setActive(boolean value) {
        isActive = value;
        System.out.println("Set alarm to active: "+value);
    }
}
